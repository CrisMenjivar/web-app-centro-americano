import React, { Component } from "react";
import "./whatsapp.css"

class Whatsapp extends Component {
  render() {
    return (
      <div className="contendor-whatsapp">
        <a title="Enviar un mensaje al whatsapp" data-toggle="tooltip" data-placement="top" target="_black" href="https://wa.me/50376292088?text=Quisiera%20consultar%20sobre%20la%20oferta%20de%20departamento`">
          <img className="btn-whatsapp-personalizado"
            src="/Empresa/whatsapp.png"
            alt="logo"
          />
        </a>
      </div>
    );
  }
}

export default Whatsapp;
