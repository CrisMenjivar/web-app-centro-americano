import React, { Component, Fragment } from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { logoutUsuario } from "../../Redux/Usuarios/actions";
import Swal from "sweetalert2";
import "./menu.css";

class Menu extends Component {
  Desloguear_Usuario = () => {
    const { dispatch } = this.props;

    Swal.fire({
      title: "Cerra sesión",
      text: `Estas seguro?`,
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si cerrar!",
    }).then((result) => {
      if (result.value) {
        dispatch(logoutUsuario());
      }
    });
  };
  render() {
    const { usuario } = this.props;
    return (
      <Fragment>
        <nav
          className="menu-principal navbar navbar-expand-lg navbar-dark bg-dark fixed-top py-0"
          id="mainNav"
        >
          <div className="container">
            <NavLink className="js-scroll-trigger" to={"/"}>
              <img
                className="img-fluid "
                src="/Empresa/logo_2.png"
                alt="logo"
                style={{ width: "90px", height: "80px" }}
              />
            </NavLink>
            <button
              className="navbar-toggler navbar-toggler-right"
              type="button"
              data-toggle="collapse"
              data-target="#navbarResponsive"
              aria-controls="navbarResponsive"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              Menu
              <i className="fas fa-bars"></i>
            </button>
            <div className="collapse navbar-collapse" id="navbarResponsive">
              <ul className="navbar-nav text-uppercase ml-auto">
                <li className="nav-item">
                  <NavLink
                    className="nav-link js-scroll-trigger"
                    to={"/contactos"}
                  >
                    Contactos
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link js-scroll-trigger"
                    to={"/cursos"}
                  >
                    Cursos
                  </NavLink>
                </li>

                <li className="nav-item">
                  <NavLink
                    className="nav-link js-scroll-trigger"
                    to={"/galerias"}
                  >
                    Galerías
                  </NavLink>
                </li>

                {usuario.access_token && usuario.is_logued && (
                  <li className="nav-item">
                    <NavLink
                      className="nav-link js-scroll-trigger"
                      to={"/dashboard"}
                    >
                      dashboard
                    </NavLink>
                  </li>
                )}
              </ul>

              <ul className="navbar-nav">
                <li className="nav-item">
                  {usuario.access_token && usuario.is_logued ? (
                    <button
                      className="btn btn-outline-primary"
                      onClick={() => this.Desloguear_Usuario()}
                    >
                      <i class="fa fa-user-times mx-1" aria-hidden="true"></i>
                      {`Hola, ${usuario.nombre}`}
                    </button>
                  ) : (
                    <NavLink
                      className="btn btn-outline-primary js-scroll-trigger"
                      to={"/login"}
                    >
                      <i class="fa fa-user mx-1" aria-hidden="true"></i>
                      Login
                    </NavLink>
                  )}
                </li>
              </ul>
              <ul className="navbar-nav">
                <li className="nav-item"></li>
              </ul>
            </div>
          </div>
        </nav>
      </Fragment>
    );
  }
}

//mapea y convierte en propiedades
const mapStateToProps = (state) => ({
  //usuario logeado.
  usuario: state.usuario,
});
//conectando. funcion sobre funcion
export default connect(mapStateToProps)(Menu);
