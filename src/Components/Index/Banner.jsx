import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./banner.css";
export default class Banner extends Component {
  render() {
    return (
      <>
        <section className="bg-color text-center padd ">
          <div clasname="row justify-content-center">
            <div clasname="col-11 col-sm-10 col-md-9 col-xl-7">
              <h3 className="texto3 display-4">Bienvenido</h3>
              <h2 className="display-2 texto3 ">Centro Académico CR</h2>
            </div>
          </div>
          <Link
            class="btn btn-warning btn-xl text-uppercase pt-3 "
            to="/contactos"
          >
            Contacto
          </Link>
        </section>
      </>
    );
  }
}
