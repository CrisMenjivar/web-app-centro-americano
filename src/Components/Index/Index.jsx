import React, { Component, Fragment } from "react";
import Banner from "./Banner";
//import AlumnoIcon from "./iconos/alumnos.jpg";

import Empresa from "../Empresa/Empresa";
export default class Index extends Component {
  render() {
    return (
      <Fragment>
        <Banner />
        <Empresa />
      </Fragment>
    );
  }
}
