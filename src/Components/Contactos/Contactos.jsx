import React, { Component } from "react";
import Axios from "axios";
import { API_BASE } from "../Endpoint/Endpoints";
import Swal from "sweetalert2";

class Contacto extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nombre: "",
      apellido: "",
      email: "",
      telefono: "",
      asunto: "",
    };
  }

  sendMessage = () => {
    const { nombre, apellido, email, telefono, asunto } = this.state;

    console.log(nombre, apellido, email, telefono, asunto, API_BASE);

    Axios.post(
      `${API_BASE}/contactos`,
      {
        nombre: nombre,
        apellido: apellido,
        email: email,
        telefono: telefono,
        asunto: asunto,
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
      .then((response) => {
        if (response.data) {
          this.setState({
            nombre: "",
            apellido: "",
            email: "",
            telefono: "",
            asunto: "",
          });

          Swal.fire("Contacto", `¡Tu mensaje se ha enviado con éxito!`, "success");
        }
      })
      .catch((err) => Swal.fire("Contacto", `¡Upp hay ocurrido un error, intenta nuevamente!`, "error"));
  };

  render() {
    const { nombre, apellido, email, telefono, asunto } = this.state;
    return (
      <div className="my-5">
        {/* <ContactoHeader /> */}

        <section className="page-section" id="contact">
          <div className="container">
            <div className="row">
              <div className="col-lg-12 text-center">
                <h2 className="section-heading text-uppercase">Contactanos</h2>
                <h3 className="section-subheading text-muted">
                  Ingresa tus datos para ponernos en contacto con tigo.
                </h3>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12">
                <div
                  id="contactForm"
                  name="sentMessage"
                  novalidate="novalidate"
                >
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <input
                          className="form-control"
                          id="name"
                          type="text"
                          placeholder="Tu Nombre *"
                          required="required"
                          data-validation-required-message="Por favor ingrese su nombre."
                          onChange={(e) =>
                            this.setState({ nombre: e.target.value })
                          }
                          value={nombre}
                        />
                        <p className="help-block text-danger"></p>
                      </div>

                      <div className="form-group">
                        <input
                          className="form-control"
                          id="apellido"
                          type="text"
                          placeholder="Apellido *"
                          required="required"
                          data-validation-required-message="Por favor ingrese su apellido."
                          onChange={(e) =>
                            this.setState({ apellido: e.target.value })
                          }
                          value={apellido}
                        />
                        <p className="help-block text-danger"></p>
                      </div>
                      <div className="form-group">
                        <input
                          className="form-control"
                          id="email"
                          type="email"
                          placeholder="Tu Email *"
                          required="required"
                          data-validation-required-message="Por favor ingrese su correo electronico."
                          onChange={(e) =>
                            this.setState({ email: e.target.value })
                          }
                          value={email}
                        />
                        <p className="help-block text-danger"></p>
                      </div>
                      <div className="form-group">
                        <input
                          class="form-control"
                          id="phone"
                          type="tel"
                          placeholder="Tu Telefono *"
                          required="required"
                          data-validation-required-message="Por favor ingrese su numero de telefono."
                          onChange={(e) =>
                            this.setState({ telefono: e.target.value })
                          }
                          value={telefono}
                        />
                        <p className="help-block text-danger"></p>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group">
                        <textarea
                          class="form-control"
                          id="message"
                          placeholder="Tu Mensaje *"
                          required="required"
                          data-validation-required-message="Por favor ingrese su mensaje."
                          onChange={(e) =>
                            this.setState({ asunto: e.target.value })
                          }
                          value={asunto}
                        ></textarea>
                        <p className="help-block text-danger"></p>
                      </div>
                    </div>
                    <div className="clearfix"></div>
                    <div className="col-lg-12 text-center">
                      <div id="success"></div>
                      <button
                        id="sendMessageButton"
                        className="btn btn-warning btn-xl text-uppercase"
                        type="submit"
                        onClick={() => this.sendMessage()}
                      >
                        Enviar Mensaje
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default Contacto;
