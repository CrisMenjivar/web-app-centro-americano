import React, { Component } from "react";
import "./empresa.css";

export default class Empresa extends Component {
  constructor(props) {
    super(props);
    this.state = {
      empresa: {
        quienesSomos: "¿Quienes somos?",
        mision: "Nuestra mision",
        vision: "Nuestra vision",
        email: "Email@.com",
        telefono: "50388998877",
      },
    };
  }

  render() {
    return (
      <section className="page-section fondo py-0 " id="services">
        <div
          className="  alert alert-primary rounded-0 text-center text-dark"
          role="alert"
        >
          En Centro Academico CR Te guiamos hacia tu futuro
        </div>

        <div className="container  ">
          <div className="row text-center">
            <div className="col-md-4">
              <div className="team-member">
                <img className="mx-auto rounded" src="img/team/1.jpg" alt="" />
              </div>
              <h4 className="service-heading texto2 ">QUIENES SOMOS </h4>
              <p className="texto2">{this.state.empresa.quienesSomos}</p>
            </div>
            <div className="col-md-4">
              <div className="team-member">
                <img className="mx-auto rounded" src="img/team/1.jpg" alt="" />
              </div>
              <h4 className="service-heading texto2">MISION </h4>
              <p className="texto2">{this.state.empresa.mision}</p>
            </div>
            <div className="col-md-4">
              <div className="team-member">
                <img className="mx-auto rounded" src="img/team/1.jpg" alt="" />
              </div>
              <h4 className="service-heading texto2">VISION</h4>
              <p className="texto2">{this.state.empresa.vision}</p>
            </div>
            <div className="col-md-6">
              <div className="team-member">
                <img className="mx-auto rounded" src="img/team/1.jpg" alt="" />
              </div>
              <h4 className="service-heading texto2">TELEFONO </h4>
              <p className="texto2">{this.state.empresa.telefono}</p>
            </div>

            <div className="col-md-6">
              <div className="team-member">
                <img className="mx-auto rounded" src="img/team/1.jpg" alt="" />
              </div>
              <h4 className="service-heading texto2">EMAIL </h4>
              <p className="texto2">{this.state.empresa.email}</p>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
