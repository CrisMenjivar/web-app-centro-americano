import React, { Component } from "react";
import Swal from "sweetalert2";
import { API_BASE } from "../Endpoint/Endpoints";
import Axios from "axios";
import { withRouter } from "react-router-dom";
import store from "../../Redux/Store";
import { loginUsuario } from "../../Redux/Usuarios/actions";
import "./estilos.css";
import { isNoAutorizado } from "../../Services/ErroresServices";

const qs = require("querystring");

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
    };
  }

  authentication = () => {
    const { username, password } = this.state;
    const { history } = this.props;

    if (username === "" || password === "") {
      Swal.fire("Error al loguearse!", "Hay datos vacios!", "warning");
      return;
    }

    //creadenciales de react para poder autenticarse con spring
    const credenciales = btoa(`reactapp:12345`);

    const requestBody = {
      grant_type: "password",
      username: username,
      password: password,
    };

    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Basic " + credenciales,
      },
    };

    Axios.post(`${API_BASE}/oauth/token`, qs.stringify(requestBody), config)
      .then((response) => {
        console.log(response);
        if (response.data) {
          const {
            access_token,
            token_type,
            refresh_token,
            expires_in,
            scope,
            info_adicional,
            apellido,
            nombre,
          } = response.data;

          //guardar en local storage
          try {
            localStorage.setItem("access_token", access_token);
            localStorage.setItem("token_type", token_type);
            localStorage.setItem("refresh_token", refresh_token);
            localStorage.setItem("expires_in", expires_in);
            localStorage.setItem("scope", scope);
            localStorage.setItem("info_adicional", info_adicional);
            localStorage.setItem("apellido", apellido);
            localStorage.setItem("nombre", nombre);
          } catch (error) {}

          store.dispatch(loginUsuario(response.data));

          Swal.fire("Login success!", `Hola, bienvenido ${nombre}`, "success");

          //redirigir a dashboard
          history.push("/dashboard");
        }
      })
      .catch((err) => {
        isNoAutorizado(err);
      });
  };

  render() {
    return (
      <section className="page-section estilo pt-5">
        <div className="Container">
          <div className="row justify-content-center pt-5 mt-5 mr-1">
            <div className="col-md-6 col-sm-8 col-xl-4 col-lg-5 formulario">
              <div action="form-group row">
                <div className="form-group text-center">
                  <h2 className="text-ligt">INICIAR SESIÓN</h2>
                </div>
                <h5 className="text-ligt">Usuario:</h5>
                <div className="form-group mx-sm-4 pt-3">
                  <input
                    type="text"
                    id="username"
                    className="form-control"
                    placeholder="Ingrese su Usuario"
                    onChange={(e) =>
                      this.setState({ username: e.target.value })
                    }
                  />
                </div>

                <label htmlFor="password ">
                  <h5 className="text-ligt">Contraseña:</h5>
                </label>
                <div className="form-group mx-sm-4 pb-3">
                  <input
                    type="password"
                    id="password"
                    className="form-control"
                    placeholder="Ingrese su Contraseña"
                    onChange={(e) =>
                      this.setState({ password: e.target.value })
                    }
                  />
                </div>

                <div className="form-group mx-sm-4 pb-2">
                  <button
                    className="btn btn-dark btn-block ingresar"
                    onClick={() => this.authentication()}
                  >
                    Ingresar
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default withRouter(Login);
