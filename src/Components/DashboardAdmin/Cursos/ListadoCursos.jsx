import React, { Component } from 'react'
import "./listado_cursos.css"
import { connect } from 'react-redux';
import {  ELiminar_curso } from '../../../Redux/Cursos/actions';
import Modal from '../../Modal/Modal';
import CrearCursos from './CrearCursos';
import { API_BASE } from '../../Endpoint/Endpoints';
import EliminarIcon from "../../Iconos/basura.svg"
import EditarIcon from "../../Iconos/editar.svg"
// import NuevoIcon from "../../Iconos/nuevo.svg"
import Swal from 'sweetalert2';

class ListadoCursos extends Component {
    constructor(props){
        super(props);
        this.state={
            showModal : false,
            curso_selected : null
        }

        this.cerra_modal = this.cerra_modal.bind(this)
    }

    cerra_modal=()=>{
        this.setState({
            showModal : false,
            curso_selected : null
        })
    }

    abrir_modal=()=>{
        this.setState({
            showModal : true
        })
    }

    Editar_Curso = (curso) => {
        this.setState({
            curso_selected : curso
        })

        this.abrir_modal();
    }

    Eliminar_curso=(curso)=>{
        Swal.fire({
            title: "Eliminar curso",
            text: `Estas seguro de eliminar ${curso.titulo}?`,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Si eliminar!"
          }).then(result => {
            if (result.value) {
                const { dispatch } = this.props
                dispatch(ELiminar_curso(curso.id));
            }
        })
    }

    render() {
        const { cursos } = this.props;
        const { showModal, curso_selected } = this.state

        return (
            <div className="">
                 <div className="contenedor-titulo-cursos">
                    <h2 className="my-2 text-info">Mis cursos {`(${cursos.length})`}</h2>

                    <button className="btn btn-success" onClick={()=> this.abrir_modal()}>
                        {/* <img style={{width:"20px", marginRight:"4px"}} src={NuevoIcon} alt={NuevoIcon}></img> */}
                     Crear nuevo curso</button>
                </div>


                <div className="tabla-de-cursos table-responsive">
                <table className="table table-hover">
                    <thead className="bg-info text-white">
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Titulo</th>
                            <th scope="col">Descripcion</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            cursos && cursos.length > 0 ?
                                cursos.map(c =>
                                    <tr key={c.id}>
                                        <td style={{width: "50px"}}>
                                            <img style={{width: "30px", height:"30px"}} src={`${API_BASE}/cursos/ver-imagen/${c.id}?param=${c.imagenHashCode}`} alt={`imagen-${c.imagenHashCode}`} />
                                        </td>
                                        <td>{c.titulo}</td>
                                        <td>{c.descripcion}</td>
                                        <td style={{width: "50px"}}>
                                            <button className="btn btn-outline-warning btn-sm" onClick={()=> this.Editar_Curso(c)}
                                              data-toggle="tooltip" data-placement="top" title="editar curso." ><img style={{width:"20px"}} src={EditarIcon} alt="editar"/></button>
                                        </td>
                                        <td style={{width: "50px"}}>
                                            <button className="btn btn-outline-danger btn-sm" onClick={()=> this.Eliminar_curso(c)}
                                                data-toggle="tooltip" data-placement="top" title="Eliminar curso"><img style={{width:"20px"}} src={EliminarIcon} alt="eliminar"/></button>
                                        </td>
                                    </tr>
                                )
                            : "No tienes cursos publicados."
                        }
                    </tbody>
                    </table>
                </div>

                {
                    showModal && <Modal closeModal={this.cerra_modal} show={showModal} modalType={"modal-general"}>
                        <CrearCursos closeModal={this.cerra_modal} curso={curso_selected}/>
                    </Modal>
                }
                
            </div>
        )
    }
}

const mapStateToProps = state => ({
    cursos: state.cursos.listado
});
  
export default connect(mapStateToProps)(ListadoCursos);
