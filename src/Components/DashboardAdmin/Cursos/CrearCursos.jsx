import React, { Component } from 'react'
import Swal from 'sweetalert2';
import Axios from 'axios';
import { API_BASE } from '../../Endpoint/Endpoints';
import { Agregar_curso_al_listado, Actualizar_curso_al_listado } from '../../../Redux/Cursos/actions';
import store from '../../../Redux/Store';
import { isNoAutorizado } from '../../../Services/ErroresServices';
import LoadingButton from '../Animaciones/LoadingButton';

export default class CrearCursos extends Component {
    constructor(props){
        super(props);
        this.state = {
            curso: {
                id: null,
                titulo: "",
                descripcion: "",
                urlCurso: ""
            },
            imagen: null,
            imagen_url : null,
            is_loading: false
        }
    }

    componentWillMount(){
        const { curso } = this.props
        if(curso){
            this.setState({curso})
        }
    }

    onChangeImage = (e) =>{
        const imagen = e.target.files[0]

        let imagen_url = null;
        if(imagen){
            imagen_url =  URL.createObjectURL(imagen)
        }
        this.setState({
            imagen: imagen,
            imagen_url: imagen_url
        })
    }

    onChange =({target})=>{
        const { name, value } = target
        let curso = Object.assign({}, this.state.curso)
        curso[name] = value;
        this.setState({
            curso: curso
        })
    }

    Validaciones=()=>{
        const { curso, imagen } = this.state
        const { titulo } = curso

        if(titulo===""){
            Swal.fire("Error", `¡Debe agregar un titulo!`, "warning");
            return false;
        }

        if(!imagen){
            Swal.fire("Error", `¡Debe agregar una imagen!`, "warning");
            return false;
        }

        return true;
    }


    Guardar_Curso =()=>{
        const { closeModal } = this.props
        const { curso, imagen } = this.state

        if(!this.Validaciones()){
            return;
        }

        this.setState({is_loading: true})

        const access_token = localStorage.getItem("access_token")

        let url = `${API_BASE}/cursos`;
        let content_type = 'multipart/form-data';

        // if(curso.id && !imagen){
        //     url = `${API_BASE}/cursos/editar/sin-imagen`;
        //     content_type = 'application/json';
        // }

        const config = {
            headers: {
                'Authorization': `Bearer ${access_token}`,
                'content-type': content_type
            }
        }

        const formData = new FormData();

        if(curso.id){
            formData.append('id', curso.id)
        }

        formData.append('imagenFile', imagen ? imagen : null)
        formData.append('titulo', curso.titulo)
        formData.append('descripcion', curso.descripcion)
        formData.append('urlCurso', curso.urlCurso)

        if(curso.id){
            Axios.put(`${url}/${curso.id}`, formData,config).then((response)=>{
                store.dispatch(Actualizar_curso_al_listado(response.data));
                Swal.fire("Editar curso!", `¡Curso editadó con éxito!`, "success");
                this.setState({is_loading: false})
                closeModal();
             }).catch((err) =>{
                this.setState({is_loading: false}) 
                isNoAutorizado(err)
             });
        }else{
            Axios.post(url, formData,config).then((response)=>{
                store.dispatch(Agregar_curso_al_listado(response.data));
                Swal.fire("Guardar curso!", `¡Curso guardado con éxito!`, "success");

                this.setState({is_loading: false})
                closeModal();
            }).catch((err) =>{
                this.setState({is_loading: false})
                isNoAutorizado(err)
            });
        }
    }

    render() {
        const { closeModal } = this.props
        const { curso, imagen_url, is_loading } = this.state

        return (
            <div className="container">

                <h3 className="text-info text-center border-bottom"> {`${curso.id ? 'Editar': 'Crear'}`} curso</h3>

                <div>
                    <div class="form-group">
                        <label htmlFor="exampleFormControlInput1">Titulo</label>
                        <input type="text" className="form-control" name="titulo" placeholder="Titulo" value={curso.titulo} onChange={this.onChange} required/>
                    </div>
                    <div class="form-group">
                        <label htmlFor="descripcion">descripcion</label>
                        <textarea className="form-control" name="descripcion" rows="2" placeholder="Escribe una breve descripcion" value={curso.descripcion} onChange={this.onChange}></textarea>
                    </div>
                    <div class="form-group">
                        <label htmlFor="urlCurso">UrlCurso</label>
                        <input type="text" className="form-control" name="urlCurso" placeholder="UrlCurso" value={curso.urlCurso} onChange={this.onChange} />
                    </div>
                    <div className="form-group seleccionar-imagen">
                        {
                            imagen_url && <img className="img-thumbnail imagen-preview" src={imagen_url} alt={imagen_url} />
                        }
                        <div>
                            <label htmlFor="urlCurso">Seleccionar imagen</label>
                            <input type="file" onChange={this.onChangeImage} required/>
                        </div>
                    </div>
                </div>

                <div className="text-center">
                        <button className="btn btn-danger mx-1" onClick={()=> closeModal()}>
                            Salir
                        </button>
                        {
                            is_loading ? 
                            <LoadingButton titulo={"Guardando"} />
                            :
                            <button className="btn btn-success mx-1" type="submit" onClick={()=>this.Guardar_Curso()}>
                                {`${curso.id ? 'Editar': 'Crear'}`} curso
                            </button>
                        }
                        
                    </div>
            </div>
        )
    }
}
