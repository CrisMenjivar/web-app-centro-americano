import React, { Component } from "react";
import ListadoCursos from "./Cursos/ListadoCursos";
import ListadoGalerias from "./Galerias/ListadoGalerias"
import InformacionEmpresa from "./Empresa/InformacionEmpresa"
import DashboardIndex from "./DashboardIndex";
import { VENTANA_DASHBOARD, VENTANA_CURSOS, VENTANA_EMPRESA, VENTANA_GALERIAS, VENTANA_CONTACTOS } from "./ContanstantesVentanaDashboard";
import ListadoContactos from "./Contactos/ListadoContactos";
import { Obtener_listado_de_cursos } from '../../Redux/Cursos/actions';
import { Obtener_listado_de_galerias } from '../../Redux/Galerias/actions';
import { Obtener_listado_de_contactos } from '../../Redux/Contactos/actions';
import Store from "../../Redux/Store"
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";


class Dashboard extends Component {
  constructor(props){
    super(props);
    this.state={
      Ventana_seleccionada : VENTANA_DASHBOARD
    }

    this.Cambiar_Ventana_Activa = this.Cambiar_Ventana_Activa.bind(this)
  }

  componentWillMount() {
    Store.dispatch(Obtener_listado_de_cursos());
    Store.dispatch(Obtener_listado_de_galerias());
    Store.dispatch(Obtener_listado_de_contactos());
  }

  Validar_Sesion_Usuario =()=>{
    const { usuario, history } = this.props
    if(!usuario.is_logued){
      history.push("/login")
    }
  }

  Cambiar_Ventana_Activa = (ventana) =>{
    this.setState({ Ventana_seleccionada: ventana})
  }

  Renderizar_Ventana_Activa = () =>{
    const { Ventana_seleccionada } = this.state
    
    switch (Ventana_seleccionada) {
      case VENTANA_DASHBOARD:
        return <DashboardIndex Cambiar_Ventana_Activa={this.Cambiar_Ventana_Activa} />

      case VENTANA_CURSOS:
        return <ListadoCursos Cambiar_Ventana_Activa={this.Cambiar_Ventana_Activa} />

      case VENTANA_EMPRESA:
        return <InformacionEmpresa Cambiar_Ventana_Activa={this.Cambiar_Ventana_Activa} />

      case VENTANA_GALERIAS:
        return <ListadoGalerias Cambiar_Ventana_Activa={this.Cambiar_Ventana_Activa} />

      case VENTANA_CONTACTOS:
        return <ListadoContactos />
    
      default:
        break;
    }
  }

  render(){
    //validar usuario
    this.Validar_Sesion_Usuario()

    const {Ventana_seleccionada} = this.state

    return (
      <div className="container" style={{marginTop: "20vh"}}>

        {
          Ventana_seleccionada!==VENTANA_DASHBOARD && 
          <button className="btn btn-outline-info my-2" onClick={()=> this.Cambiar_Ventana_Activa(VENTANA_DASHBOARD)}>
            Regresar a dashboard
          </button>
        }

        {
          this.Renderizar_Ventana_Activa()
        }

      </div>
    );
  }
}

//mapea y convierte en propiedades
const mapStateToProps = (state) => ({
  //usuario logeado.
  usuario: state.usuario,
});
//conectando. funcion sobre funcion
export default connect(mapStateToProps)(withRouter(Dashboard));
