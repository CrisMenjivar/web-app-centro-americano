import React from 'react';

function LoadingButton({titulo}) {
    return (
        <button class="btn btn-success" type="button" disabled>
        <span class="spinner-border spinner-border-sm mx-2" role="status" aria-hidden="true"></span>
            {titulo}
        </button>
    );
}

export default LoadingButton;