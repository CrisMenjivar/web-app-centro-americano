import React, { Component } from 'react'
import Axios from 'axios';
import { API_BASE } from '../../Endpoint/Endpoints';
import { Actualizar_Contacto } from '../../../Redux/Contactos/actions';
import Store from "../../../Redux/Store"

class VerContacto extends Component {

    Marcar_contacto_visto =()=>{
        const { contacto } = this.props

        if(contacto.leido){
            return;
        }

        const access_token = localStorage.getItem("access_token")

        const url = `${API_BASE}/contactos/visto/${contacto.id}`;
        const config = {
            headers: {
                'Authorization': `Bearer ${access_token}`,
                "Content-Type": "application/json"
            }
        }

        Axios.post(url, {}, config).then((response)=>{
            Store.dispatch(Actualizar_Contacto(response.data))
        }).catch((err) =>
            console.log("error al marcar como visto: " + err)
        );
    }

    componentDidMount(){
        this.Marcar_contacto_visto();
    }

    render(){
        const { contacto } = this.props

        return (
            <div className="container">
                <h3 className="text-info border-bottom">Información del contacto</h3>
    
                <div className="my-2">
                    <p className="text-muted">{`Nombre: ${contacto.nombre} ${contacto.apellido}`}</p>
                    <p className="text-muted">{`Telefono: ${contacto.telefono}`}</p>
                    <p className="text-muted">{`Telefono: ${contacto.email}`}</p>
                </div>
    
                <div className="my-2">
                    <p className="text-muted">Asunto:</p>
                    <p className="text-dark">{`${contacto.asunto}`}</p>
                </div>
            </div>
        )
    }
}

export default VerContacto
