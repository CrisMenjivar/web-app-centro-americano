import React, { Component } from "react";
import "./listado_contactos.css";
import { connect } from "react-redux";
import Modal from "../../Modal/Modal";
import EliminarIcon from "../../Iconos/basura.svg";
import EditarIcon from "../../Iconos/editar.svg";
import LeidoIcon from "../../Iconos/leido.png";
import NOLeidoIcon from "../../Iconos/no_leido.png";
import Swal from "sweetalert2";
import { Eliminar_Contacto } from "../../../Redux/Contactos/actions";
import VerContacto from "./VerContacto";

class ListadoContactos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      contacto_selected: null,
    };

    this.cerra_modal = this.cerra_modal.bind(this);
  }

  cerra_modal = () => {
    this.setState({
      showModal: false,
      curso_selected: null,
    });
  };

  abrir_modal = () => {
    this.setState({
      showModal: true,
    });
  };

  ver_contacto = (contacto) => {
    this.setState({
      contacto_selected: contacto,
    });

    this.abrir_modal();
  };

  Eliminar_contacto = (contacto) => {
    Swal.fire({
      title: "Eliminar contacto",
      text: `Estas seguro de eliminar ${contacto.id}?`,
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si eliminar!",
    }).then((result) => {
      if (result.value) {
        const { dispatch } = this.props;
        dispatch(Eliminar_Contacto(contacto.id));
      }
    });
  };

  render() {
    const { contactos } = this.props;
    const { showModal, contacto_selected } = this.state;

    let contactos_ordenados = [];

    const contactos_no_leidos = contactos.filter((c) => !c.leido);
    const contactos_leidos = contactos.filter((c) => c.leido);

    contactos_ordenados = contactos_no_leidos.concat(contactos_leidos);

    return (
      <div className="">
        <div className="contenedor-titulo-contactos">
          <h2 className="my-2 text-info">
            Mis contactos {`(${contactos_ordenados.length})`}
          </h2>
        </div>

        {contactos_no_leidos.length > 0 && (
          <h4 className="text-danger">{`${contactos_no_leidos.length} mensaje${
            contactos_no_leidos.length > 1 ? "s" : ""
          } sin leer`}</h4>
        )}

        <div className="tabla-de-contactos table-responsive">
          <table className="table table-hover">
            <thead className="bg-info text-white">
              <tr>
                <th scope="col"></th>
                <th scope="col">Nombre</th>
                <th scope="col">Apellido</th>
                <th scope="col">Email</th>
                <th scope="col">Teléfono</th>
                <th scope="col"></th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              {contactos_ordenados && contactos_ordenados.length > 0
                ? contactos_ordenados.map((c) => (
                    <tr key={c.id}>
                      <td style={{ width: "50px" }}>
                        <img
                          className="status-mensaje dame-click"
                          src={c.leido ? LeidoIcon : NOLeidoIcon}
                          alt="estatus contacto"
                          data-toggle="tooltip"
                          data-placement="top"
                          title={c.leido ? "Mensaje leido" : "Mensaje no leido"}
                          onClick={() => this.ver_contacto(c)}
                        />
                      </td>
                      <td>{c.nombre}</td>
                      <td>{c.apellido}</td>
                      <td>{c.email}</td>
                      <td>{c.telefono}</td>
                      <td>
                        <button
                          className="btn btn-outline-info btn-sm"
                          onClick={() => this.ver_contacto(c)}
                          data-toggle="tooltip"
                          data-placement="top"
                          title="ver contacto"
                        >
                          <img
                            style={{ width: "20px" }}
                            src={EditarIcon}
                            alt="ver"
                          />{" "}
                          ver detalle
                        </button>
                      </td>
                      <td>
                        <button
                          className="btn btn-outline-danger btn-sm"
                          onClick={() => this.Eliminar_contacto(c)}
                          data-toggle="tooltip"
                          data-placement="top"
                          title="Eliminar contacto"
                        >
                          <img
                            style={{ width: "20px" }}
                            src={EliminarIcon}
                            alt="eliminar"
                          />
                        </button>
                      </td>
                    </tr>
                  ))
                : "No tienes contactos."}
            </tbody>
          </table>
        </div>

        {showModal && (
          <Modal
            closeModal={this.cerra_modal}
            show={showModal}
            modalType={"modal-general"}
          >
            <VerContacto contacto={contacto_selected} />
          </Modal>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  contactos: state.contactos.listado,
});

export default connect(mapStateToProps)(ListadoContactos);
