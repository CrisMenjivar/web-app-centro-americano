import React, { Component } from 'react'
import Swal from 'sweetalert2';
import Axios from 'axios';
import { API_BASE } from '../../Endpoint/Endpoints';
import store from '../../../Redux/Store';
import { Actualizar_galeria, Agregar_galeria } from '../../../Redux/Galerias/actions';
import { isNoAutorizado } from '../../../Services/ErroresServices';
import LoadingButton from '../Animaciones/LoadingButton';

export default class CrearGalerias extends Component {
    constructor(props){
        super(props);
        this.state = {
            galeria : {
                id: null,
                imagenHashCode: ""
            },
            imagen: null,
            imagen_url: null,
            is_loading: false
        }
    }

    componentWillMount(){
        const { galeria } = this.props
        if(galeria){
            this.setState({galeria})
        }
    }

    onChangeImage = (e) =>{
        const imagen = e.target.files[0]
        let imagen_url = null;
        if(imagen){
            imagen_url =  URL.createObjectURL(imagen)
        }
        
        this.setState({
            imagen: imagen,
            imagen_url: imagen_url
        })
    }

    onChange =(e)=>{
        let galeria = Object.assign({}, this.state.galeria)
        galeria[e.target.name] = e.target.value;
        this.setState({
            galeria: galeria
        })
    }

    Validaciones=()=>{
        const {  imagen } = this.state
        
        if(!imagen){
            Swal.fire("Error", `¡Debe agregar una imagen!`, "warning");
            return false;
        }

        return true;
    }

    Guardar_galeria =()=>{
        const { closeModal } = this.props
        const { galeria, imagen } = this.state

        if(!this.Validaciones()){
            return;
        }

        this.setState({is_loading : true})

        const access_token = localStorage.getItem("access_token")

        const url = `${API_BASE}/galerias`;
        const config = {
            headers: {
                'Authorization': `Bearer ${access_token}`,
                'content-type': 'multipart/form-data'
            }
        }

        const formData = new FormData();

        if(galeria.id){
            formData.append('id', galeria.id)
        }
        formData.append('imagenFile', imagen ? imagen : "")

        if(galeria.id){
            Axios.put(`${url}/${galeria.id}`, formData,config).then((response)=>{
                store.dispatch(Actualizar_galeria(response.data));
                Swal.fire("Editar galerias!", `galeria editada con éxito!`, "success");

                this.setState({is_loading : false})
                closeModal();
             }).catch((err) =>{
                this.setState({is_loading : false})  
                isNoAutorizado(err)
             });
        }else{
            Axios.post(url, formData,config).then((response)=>{
                store.dispatch(Agregar_galeria(response.data));
                Swal.fire("Guardar galeria!", `Galeria guardada con éxito!`, "success");

                this.setState({is_loading : false})
                closeModal();
             }).catch((err) =>{
                this.setState({is_loading : false})  
                isNoAutorizado(err)
             });
        }
    }

    render() {
        const { closeModal } = this.props
        const { galeria, imagen_url, is_loading } = this.state

        return (
            <div className="container">

                <h3 className="text-info text-center border-bottom"> {`${galeria.id ? 'Editar': 'Crear'}`} galeria</h3>

                <div className="my-2">

                    <div className="form-group">
                        {
                            imagen_url && <img style={{width: "200px"}} className="img-thumbnail" src={imagen_url} alt={imagen_url} />
                        }
                        
                    </div>

                    <div className="form-group">
                        <label htmlFor="urlCurso">Seleccionar imagen</label>
                        <input type="file" onChange={this.onChangeImage} required/>
                    </div>
                </div>

                <div className="text-center">
                        <button className="btn btn-danger mx-1" onClick={()=> closeModal()}>
                            Salir
                        </button>

                        {
                            is_loading ? 
                            <LoadingButton titulo={"Guardando"} />
                            :
                            <button className="btn btn-success mx-1" type="submit" onClick={()=>this.Guardar_galeria()}>
                                {`${galeria.id ? 'Editar': 'Crear'}`} galeria
                            </button>
                        }
                        
                    </div>
            </div>
        )
    }
}
