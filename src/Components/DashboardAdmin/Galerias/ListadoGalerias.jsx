import React, { Component } from "react";
import "./listado_galerias.css";
import { connect } from "react-redux";
import Modal from "../../Modal/Modal";
import CrearGalerias from "./CrearGalerias";
import { API_BASE } from "../../Endpoint/Endpoints";
import EliminarIcon from "../../Iconos/basura.svg";
import EditarIcon from "../../Iconos/editar.svg";
import Swal from "sweetalert2";
import { Eliminar_galeria } from "../../../Redux/Galerias/actions";
import { isNoAutorizado } from "../../../Services/ErroresServices";

class ListadoGalerias extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      galeria_selected: null,
    };

    this.cerra_modal = this.cerra_modal.bind(this);
  }

  cerra_modal = () => {
    this.setState({
      showModal: false,
      galeria_selected: null,
    });
  };

  abrir_modal = () => {
    this.setState({
      showModal: true,
    });
  };

  Editar_Galeria = (galeria) => {
    this.setState({
      galeria_selected: galeria,
    });

    this.abrir_modal();
  };

  Eliminar_Galeria = (galeria) => {
    Swal.fire({
      title: "Eliminar galeria",
      text: `Estas seguro de eliminar la galeria?`,
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si eliminar!",
    })
      .then((result) => {
        if (result.value) {
          const { dispatch } = this.props;
          dispatch(Eliminar_galeria(galeria.id));
        }
      })
      .catch((err) => isNoAutorizado(err));
  };

  render() {
    const { galerias } = this.props;
    const { showModal, galeria_selected } = this.state;

    return (
      <div className="">
        <div className="contenedor-titulo-galerias">
          <h2 className="my-2 text-info">
            Mis Galerias {`(${galerias.length})`}
          </h2>
          <button
            className="btn btn-success"
            onClick={() => this.abrir_modal()}
          >
            {/* <img style={{width:"20px", marginRight:"4px"}} src={NuevoIcon} alt={NuevoIcon}></img> */}
            Crear nueva galeria
          </button>
        </div>

        {galerias && galerias.length > 0 ? (
          <div className="row my-3">
            {galerias.map((galeria) => (
              <div key={galeria.id} className="col-md-4">
                <div className="targeta-de-galerias">
                  <img
                    className="imagen-targeta img-thumbnail"
                    src={`${API_BASE}/galerias/ver-imagen/${galeria.id}?param=${galeria.imagenHashCode}`}
                    alt={`imagen-${galeria.imagenHashCode}`}
                  />

                  {/* <p className="text-info">{`Publicado en ${galeria.createAt}`}</p> */}

                  <div className="opciones-targeta-galeria">
                    <button
                      className="btn btn-info btn-sm mx-1"
                      onClick={() => this.Editar_Galeria(galeria)}
                      data-toggle="tooltip"
                      data-placement="top"
                      title="editar galeria."
                    >
                      <img
                        style={{ width: "20px" }}
                        src={EditarIcon}
                        alt="editar"
                      />
                    </button>
                    <button
                      className="btn btn-danger btn-sm"
                      onClick={() => this.Eliminar_Galeria(galeria)}
                      data-toggle="tooltip"
                      data-placement="top"
                      title="Eliminar galeria"
                    >
                      <img
                        style={{ width: "20px" }}
                        src={EliminarIcon}
                        alt="eliminar"
                      />
                    </button>
                  </div>
                </div>
              </div>
            ))}
          </div>
        ) : (
          <div className="alert-alert-info">No hay galerias publicadas!</div>
        )}

        {showModal && (
          <Modal
            closeModal={this.cerra_modal}
            show={showModal}
            modalType={"modal-general"}
          >
            <CrearGalerias
              closeModal={this.cerra_modal}
              galeria={galeria_selected}
            />
          </Modal>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  galerias: state.galerias.listado,
});

export default connect(mapStateToProps)(ListadoGalerias);
