import React, { Component, Fragment } from "react";
import {
  VENTANA_GALERIAS,
  VENTANA_CONTACTOS,
  VENTANA_CURSOS,
} from "./ContanstantesVentanaDashboard";
import { connect } from "react-redux";

class DashboardIndex extends Component {
  render() {
    const { Cambiar_Ventana_Activa, cursos, galerias, contactos } = this.props;

    return (
      <Fragment>
        <h1 className="text-center text-secondary border-bottom border-success">
          Administración del sitio web
        </h1>

        <div className="my-3"></div>

        <div className="row">
          <div className="col-md-4">
            <div
              className="card border border-success dame-click my-2"
              onClick={() => Cambiar_Ventana_Activa(VENTANA_GALERIAS)}
            >
              <div className="card-header bg-success text-white">{`Galerias (${galerias.length})`}</div>
              <div className="card-body">
                Listar, crear, editar y eliminar galerias
              </div>
            </div>
          </div>

          <div className="col-md-4">
            <div
              className="card border border-dark dame-click my-2"
              onClick={() => Cambiar_Ventana_Activa(VENTANA_CONTACTOS)}
            >
              <div className="card-header bg-dark text-white">{`Contactos (${contactos.length})`}</div>
              <div className="card-body">
                Ver todos mis contactos y mensajes nuevos
              </div>
            </div>
          </div>

          <div className="col-md-4">
            <div
              className="card border border-secondary dame-click my-2"
              onClick={() => Cambiar_Ventana_Activa(VENTANA_CURSOS)}
            >
              <div className="card-header bg-secondary text-white">{`Cursos (${cursos.length})`}</div>
              <div className="card-body">
                Listar, crear, editar y eliminar galerias
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  cursos: state.cursos.listado,
  galerias: state.galerias.listado,
  contactos: state.contactos.listado,
});

export default connect(mapStateToProps)(DashboardIndex);
