let API = "";

if (process.env.NODE_ENV === "production" && "serviceWorker" in navigator) {
  //production
  API = "https://backend-api-centro-americano.herokuapp.com";
} else {
  //development
  // API = "http://localhost:8080";
  API = "https://backend-api-centro-americano.herokuapp.com";
}

//endpoint base
export const API_BASE = API;
