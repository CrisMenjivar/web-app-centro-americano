import React, { Component } from "react";
import  "./Modal.css";

class Modal extends Component {

  render() {
    const { children, closeModal } = this.props;

    return (<div className="mi-modal">
          <div className="contenedor-modal">
            <div className="modal-content">
              
              <div className="cerrar-modal">
                <button
                  className="btn-cerrar-modal"
                  onClick={()=> closeModal()}
                >
                  X
                </button>
              </div>

            {children}
      </div>
          </div>
      </div>
    );
  }
}


export default Modal;
