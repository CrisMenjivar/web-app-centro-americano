import React, { Component } from "react";
// import Whatsapp from "../Whatsapp/Whatsapp";
export default class Footer extends Component {
  render() {
    return (
      <footer class="footer">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-md-12">
              <ul class="list-inline social-buttons ">
                <li class="list-inline-item">
                  <i class="fab fa-facebook-f"></i>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}
