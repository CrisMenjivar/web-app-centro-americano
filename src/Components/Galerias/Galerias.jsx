import React, { Component } from "react";
import { Obtener_listado_de_galerias } from "../../Redux/Galerias/actions";
import { connect } from "react-redux";

import { API_BASE } from "../Endpoint/Endpoints";

class Galerias extends Component {
  constructor(props) {
    super(props);
    this.state = {
      galeriasSelected: {},
    };
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(Obtener_listado_de_galerias());
  }

  render() {
    const { galerias } = this.props;

    return (
      <>
        <div className="my-5">
          <section class="bg-light page-section" id="portfolio">
            <div class="container">
              <div class="row">
                <div class="col-lg-12 text-center">
                  <h2 class="section-heading text-uppercase ">GALERÍA</h2>
                  <h3 class="section-subheading text-muted texto1">
                    Te guiamos hacia tu futuro
                  </h3>
                </div>
              </div>
              <div class="row">
                {galerias && galerias.length > 0
                  ? galerias.map((galeria, index) => (
                      <div
                        key={index}
                        className="col-md-4 col-sm-6 portfolio-item"
                      >
                        <a
                          className="portfolio-link"
                          data-toggle="modal"
                          href="#portfolioModal"
                        >
                          <div className="portfolio-hover">
                            <div className="portfolio-hover-content">
                              <i className="fas fa-plus fa-3x"></i>
                            </div>
                          </div>
                          <img
                            style={{ width: "100%", height: "250px" }}
                            className="img-fluid"
                            src={`${API_BASE}/galerias/ver-imagen/${galeria.id}`}
                            alt={galeria.id}
                          />
                          {/* <img
                            className="img-fluid"
                            src="img/portfolio/01-thumbnail.jpg"
                            alt=""
                          /> */}
                        </a>
                      </div>
                    ))
                  : ""}
              </div>
            </div>
          </section>
          <div
            class="portfolio-modal modal fade"
            id="portfolioModal"
            tabindex="-1"
            role="dialog"
            aria-hidden="true"
          >
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                  <div class="lr">
                    <div class="rl"></div>
                  </div>
                </div>
                <div class="container">
                  <div class="row">
                    <div class="col-lg-8 mx-auto">
                      <div class="modal-body">
                        <h2 class="text-uppercase">Project Name</h2>
                        <p class="item-intro text-muted">
                          Lorem ipsum dolor sit amet consectetur.
                        </p>
                        <img
                          class="img-fluid d-block mx-auto"
                          src="img/portfolio/01-full.jpg"
                          alt=""
                        />
                        <p>
                          Use this area to describe your project. Lorem ipsum
                          dolor sit amet, consectetur adipisicing elit. Est
                          blanditiis dolorem culpa incidunt minus dignissimos
                          deserunt repellat aperiam quasi sunt officia expedita
                          beatae cupiditate, maiores repudiandae, nostrum,
                          reiciendis facere nemo!
                        </p>
                        <ul class="list-inline">
                          <li>Date: January 2017</li>
                          <li>Client: Threads</li>
                          <li>Category: Illustration</li>
                        </ul>
                        <button
                          class="btn btn-primary"
                          data-dismiss="modal"
                          type="button"
                        >
                          <i class="fas fa-times"></i>
                          Close Project
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  galerias: state.galerias.listado,
});

export default connect(mapStateToProps)(Galerias);
