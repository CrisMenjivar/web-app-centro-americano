import React, { Component } from "react";
import Contactos from "./Components/Contactos/Contactos";
import Cursos from "./Components/Cursos/Cursos";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Menu from "./Components/Menu/Menu";
import Footer from "./Components/Footer/Footer";
import Whatsapp from "./Components/Whatsapp/Whatsapp";
import Galerias from "./Components/Galerias/Galerias";
import Index from "./Components/Index/Index";

import Login from "./Components/Login/Login";
import Dashboard from "./Components/DashboardAdmin/Dashboard";
import store from "./Redux/Store";
import { Provider } from "react-redux";
import { verificarUser } from "./Redux/Usuarios/actions";

store.dispatch(verificarUser());
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          {/* El menu estara en todas partes */}
          <Menu />

          <Whatsapp />

          <Switch>
            <Route exact path="/" component={Index} />

            <Route path="/contactos" exact component={Contactos} />
            <Route path="/cursos" exact component={Cursos} />
            <Route path="/galerias" exact component={Galerias} />
            <Route path="/whatsapp" exact component={Whatsapp} />
            <Route path="/login" exact component={Login} />
            <Route path="/dashboard" exact component={Dashboard} />
          </Switch>
          {/* El footer estara en todas partes */}
          <Footer />
        </Router>
      </Provider>
    );
  }
}

export default App;
