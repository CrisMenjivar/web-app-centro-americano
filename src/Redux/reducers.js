import { connectRouter } from "connected-react-router";
import { combineReducers } from "redux";
import usuarioReducer from "./Usuarios/usuarioReducer";
import CursoReducer from "./Cursos/CursoReducer";
import GaleriaReducer from "./Galerias/GaleriaReducer";
import ContactoReducer from "./Contactos/ContactoReducer";

// llamar todos los reducers

export default history =>
  combineReducers({ router: connectRouter(history), usuario: usuarioReducer, cursos: CursoReducer, galerias: GaleriaReducer, contactos: ContactoReducer });
