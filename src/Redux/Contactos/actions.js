import Axios from "axios";

import { OBTENER_LISTA_DE_CONTACTOS, ELIMINAR_CONTACTO, ACTUALIZAR_CONTACTO } from "./actionTypes";
import { API_BASE } from "../../Components/Endpoint/Endpoints";
import Swal from "sweetalert2";
import { isNoAutorizado } from "../../Services/ErroresServices";

export const Obtener_listado_de_contactos = () => dispatch => {
  const access_token = localStorage.getItem("access_token")
  
  Axios.get(`${API_BASE}/contactos`, {
    headers: {
      'Authorization': `Bearer ${access_token}`,
      "Content-Type": "application/json",
    },
  })
    .then((response) => {
      return dispatch({
        type: OBTENER_LISTA_DE_CONTACTOS,
        contactos: response.data
      });
    })
    .catch(e => {
      isNoAutorizado(e);
    });
};


export const Eliminar_Contacto = (id) => dispatch => {
  const access_token = localStorage.getItem("access_token")

  const url = `${API_BASE}/contactos/${id}`;
  const config = {
      headers: {
          'Authorization': `Bearer ${access_token}`,
          'content-type': 'application/json'
      }
  }

  Axios.delete(url, config).then((response)=>{
    Swal.fire("Eliminar contacto!", `Contacto eliminada con éxito!`, "success");
    return dispatch({
      type: ELIMINAR_CONTACTO,
      id
    });
  }).catch(e => {
    isNoAutorizado(e);
  });
};

export const Actualizar_Contacto = (contacto) => dispatch =>{
  return dispatch({
    type: ACTUALIZAR_CONTACTO,
    contacto
  })
}



