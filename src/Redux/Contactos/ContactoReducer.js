import { OBTENER_LISTA_DE_CONTACTOS, ELIMINAR_CONTACTO, ACTUALIZAR_CONTACTO } from "./actionTypes";

const initialStore = {
  listado: []
};

export default function(state = initialStore, action) {

  switch (action.type) {
    case OBTENER_LISTA_DE_CONTACTOS: {
      return {
        ...state,
        listado: action.contactos
      };
    }

    case ACTUALIZAR_CONTACTO: {
      let { listado } = state

      const nuevo_listado = listado.map(contacto => {
        let contacto_actual = contacto;
         if(contacto_actual.id === action.contacto.id){
          contacto_actual = action.contacto
         }
         return contacto_actual
      })

      return {
        ...state,
        listado: nuevo_listado
      };
    }

    case ELIMINAR_CONTACTO: {
      let { listado } = state
      // quita el curso eliminado
      const nuevo_listado = listado.filter(contacto => contacto.id !== action.id )

      return {
        ...state,
        listado: nuevo_listado
      };
    }

    default:
      return state;
  }
}
