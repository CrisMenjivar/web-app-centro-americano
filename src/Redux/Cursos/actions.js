import Axios from "axios";

import { OBTENER_LISTA_DE_CURSOS, AGREGAR_UN_CURSO_AL_LISTADO, ACTUALIZAR_UN_CURSO_AL_LISTADO, ELIMINAR_CURSO } from "./actionTypes";
import { API_BASE } from "../../Components/Endpoint/Endpoints";
import Swal from "sweetalert2";
import { isNoAutorizado } from "../../Services/ErroresServices";

export const Obtener_listado_de_cursos = () => dispatch => {
  
  Axios.get(`${API_BASE}/cursos`, {
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => {
      return dispatch({
        type: OBTENER_LISTA_DE_CURSOS,
        cursos: response.data
      });
    })
    .catch(e => {
      isNoAutorizado(e);
    });
};

export const Agregar_curso_al_listado = (curso) => dispatch => {
  return dispatch({
    type: AGREGAR_UN_CURSO_AL_LISTADO,
    curso
  });
};

export const Actualizar_curso_al_listado = (curso) => dispatch => {
  return dispatch({
    type: ACTUALIZAR_UN_CURSO_AL_LISTADO,
    curso
  });
};

export const ELiminar_curso = (id) => dispatch => {
  const access_token = localStorage.getItem("access_token")

  const url = `${API_BASE}/cursos/${id}`;
  const config = {
      headers: {
          'Authorization': `Bearer ${access_token}`,
          'content-type': 'application/json'
      }
  }

  Axios.delete(url, config).then((response)=>{
    Swal.fire("Eliminar curso!", `¡Curso eliminado con éxito!`, "success");
    return dispatch({
      type: ELIMINAR_CURSO,
      id
    });
  }).catch(err =>
    isNoAutorizado(err)
  );
};



