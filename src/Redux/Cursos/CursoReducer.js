import { OBTENER_LISTA_DE_CURSOS, AGREGAR_UN_CURSO_AL_LISTADO, ACTUALIZAR_UN_CURSO_AL_LISTADO, ELIMINAR_CURSO } from "./actionTypes";

const initialStore = {
  listado: []
};

export default function(state = initialStore, action) {

  switch (action.type) {
    case OBTENER_LISTA_DE_CURSOS: {
      return {
        ...state,
        listado: action.cursos
      };
    }

    case AGREGAR_UN_CURSO_AL_LISTADO: {
      let { listado } = state
      listado.unshift(action.curso)
      return {
        ...state,
        listado: listado
      };
    }

    case ACTUALIZAR_UN_CURSO_AL_LISTADO: {
      let { listado } = state

      const nuevo_listado = listado.map(curso => {
        let curso_actual = curso;
         if(curso_actual.id === action.curso.id){
          curso_actual = action.curso
         }
         return curso_actual
      })

      return {
        ...state,
        listado: nuevo_listado
      };
    }

    case ELIMINAR_CURSO: {
      let { listado } = state
      // quita el curso eliminado
      const nuevo_listado = listado.filter(curso => curso.id !== action.id )

      return {
        ...state,
        listado: nuevo_listado
      };
    }

    default:
      return state;
  }
}
