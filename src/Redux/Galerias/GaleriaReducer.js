import { OBTENER_LISTA_DE_GALERIAS, AGREGAR_UNA_GALERIA, ACTUALIZAR_GALERIA, ELIMINAR_GALERIA } from "./actionTypes";

const initialStore = {
  listado: []
};

export default function(state = initialStore, action) {

  switch (action.type) {
    case OBTENER_LISTA_DE_GALERIAS: {
      return {
        ...state,
        listado: action.galerias
      };
    }

    case AGREGAR_UNA_GALERIA: {
      let { listado } = state
      listado.unshift(action.galeria)
      return {
        ...state,
        listado: listado
      };
    }

    case ACTUALIZAR_GALERIA: {
      let { listado } = state

      const nuevo_listado = listado.map(galeria => {
        let galeria_actual = galeria;
         if(galeria_actual.id === action.galeria.id){
          galeria_actual = action.galeria
         }
         return galeria_actual
      })

      return {
        ...state,
        listado: nuevo_listado
      };
    }

    case ELIMINAR_GALERIA: {
      let { listado } = state
      // quita el curso eliminado
      const nuevo_listado = listado.filter(galeria => galeria.id !== action.id )

      return {
        ...state,
        listado: nuevo_listado
      };
    }

    default:
      return state;
  }
}
