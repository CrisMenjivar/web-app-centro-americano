import Axios from "axios";

import { OBTENER_LISTA_DE_GALERIAS, AGREGAR_UNA_GALERIA, ACTUALIZAR_GALERIA, ELIMINAR_GALERIA } from "./actionTypes";
import { API_BASE } from "../../Components/Endpoint/Endpoints";
import Swal from "sweetalert2";
import { isNoAutorizado } from "../../Services/ErroresServices";

export const Obtener_listado_de_galerias = () => dispatch => {
  
  Axios.get(`${API_BASE}/galerias`, {
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => {
      return dispatch({
        type: OBTENER_LISTA_DE_GALERIAS,
        galerias: response.data
      });
    })
    .catch(e => {
      isNoAutorizado(e);
    });
};

export const Agregar_galeria = (galeria) => dispatch => {
  return dispatch({
    type: AGREGAR_UNA_GALERIA,
    galeria
  });
};

export const Actualizar_galeria = (galeria) => dispatch => {
  return dispatch({
    type: ACTUALIZAR_GALERIA,
    galeria
  });
};

export const Eliminar_galeria = (id) => dispatch => {
  const access_token = localStorage.getItem("access_token")

  const url = `${API_BASE}/galerias/${id}`;
  const config = {
      headers: {
          'Authorization': `Bearer ${access_token}`,
          'content-type': 'application/json'
      }
  }

  Axios.delete(url, config).then((response)=>{
    Swal.fire("Eliminar galeria!", `Galeria eliminada con éxito!`, "success");
    return dispatch({
      type: ELIMINAR_GALERIA,
      id
    });
  }).catch(err =>
    isNoAutorizado(err)
  );
};



