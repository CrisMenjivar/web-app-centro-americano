import {
  VERIFCAR_USUARIO,
  LOGIN_AUTHORIZACTION,
  LOGOUT_USER
} from "./actionTypes";

const initialStore = {
  //todos los estados globales
  access_token: "",
  token_type: "bearer",
  refresh_token: "",
  expires_in: 0,
  scope: "",
  info_adicional: "",
  apellido: "",
  nombre: "",
  is_logued: false
};

export default function(state = initialStore, action) {
  switch (action.type) {
    case VERIFCAR_USUARIO: {
      return {
        ...state,
        ...action.usuario,
        is_logued: true
      };
    }

    case LOGIN_AUTHORIZACTION: {
      return {
        ...state,
        ...action.usuario,
        is_logued: true
      };
    }

    case LOGOUT_USER: {
      return {
        ...{
          access_token: "",
          token_type: "bearer",
          refresh_token: "",
          expires_in: 0,
          scope: "",
          info_adicional: "",
          apellido: "",
          nombre: "",
          is_logued: false
        }
      };
    }

    default:
      return state;
  }
}
