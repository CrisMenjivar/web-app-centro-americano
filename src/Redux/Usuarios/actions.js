
import {
  LOGOUT_USER,
  VERIFCAR_USUARIO,
  LOGIN_AUTHORIZACTION
} from "./actionTypes";

// aqui estaran todas las acciones del proyecto, llevara el tipo de accion y la data necesaria
//pequeñas funciones globales

//cuando se logea y es exitose agrega al estado global el usuario
const loginUsuario = usuario => ({
  type: LOGIN_AUTHORIZACTION,
  usuario: usuario
});


// deslogear usuario
const logoutUsuario = () => dispatch => {
  localStorage.removeItem("access_token");
  localStorage.removeItem("token_type");
  localStorage.removeItem("refresh_token");
  localStorage.removeItem("expires_in");
  localStorage.removeItem("scope");
  localStorage.removeItem("info_adicional");
  localStorage.removeItem("apellido");
  localStorage.removeItem("nombre");
  return dispatch({
    type: LOGOUT_USER
  });
};

const verificarUser = () => dispatch => {
  const usuario = {
    access_token: localStorage.getItem("access_token") || "",
    token_type: localStorage.getItem("token_type") || "",
    refresh_token: localStorage.getItem("refresh_token") || "",
    expires_in: localStorage.getItem("expires_in") || 0,
    scope: localStorage.getItem("scope") || "",
    info_adicional: localStorage.getItem("info_adicional") || "",
    apellido: localStorage.getItem("apellido") || "",
    nombre: localStorage.getItem("nombre") || ""
  };

  return dispatch({
    type: VERIFCAR_USUARIO,
    usuario: usuario
  });
};

export { loginUsuario, logoutUsuario, verificarUser };
