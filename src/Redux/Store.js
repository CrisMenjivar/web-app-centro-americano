import configureStore from './configureStore';

const store = configureStore(/* Initial State */);

export default store;