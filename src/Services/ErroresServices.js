import Swal from "sweetalert2";
import { logoutUsuario } from "../Redux/Usuarios/actions";
import store from "../Redux/Store";

export const isNoAutorizado=(err)=>{
    if(err && err.response){
        const { status, data } = err.response
        const { error, error_description, message } = data

        console.log("ERRORES DE TODO TYPO")
        console.log("Error: ", status, data)

        // Credenciales incorrectas
        if(status===401){
            Swal.fire(`${error}`, `Acceso denegado, credenciales invalidas`, "warning");
            store.dispatch(logoutUsuario()); // forzar deslogeo
        }
        // acceso probido
        if(status===403){
            Swal.fire(`${error}`, `Acceso probido, no tienes permisos`, "warning");
            store.dispatch(logoutUsuario()); // forzar deslogeo
        }

        // errores
        if(status===400){
            // parametros error y message
            Swal.fire(`${error}`, `${error_description || message}`, "error");
        }

        if(status===500){
            // parametros error y message
            Swal.fire(`${error}`, `${message}`, "error");
        }
    }
}